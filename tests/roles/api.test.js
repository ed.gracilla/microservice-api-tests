'use strict'

const RESOURCE = 'roles'
global.Promise = require('bluebird')

const _ = require('lodash')
const _data = require('./_data')
const should = require('should')
const Utils = require('../utils')

const supertest = require('supertest')
const request = supertest('http://localhost:8080')

describe(`${_.startCase(RESOURCE)} API`, function () {
  let user = {}
  let config = {}
  let utils = new Utils(RESOURCE)

  let _data2 = {}

  before(function (done) {
    utils.init('admin').then(options => {
      config = options.config
      user = options.user
    }).then(() => {
      _data.post.name = 'Dummy for search'
      return request.post(`/`).set(config).send(_data.post).then(ret => ret.body)
    }).then(data => {
      _data.post.name = 'Dummy'
      _data2 = data
      done()
    }).catch(done)
  })

  after(function (done) {
    Promise.all([
      request.delete(`/${_data2._id}`).set(config),
    ]).then(() => {
      done()
    }).catch(done)
  })

  describe('# API POST /', function () {
    it('should create a new record', function (done) {
      request.post(`/`).set(config).send(_data.post)
        .expect(201, (err, res) => {
          should.ifError(err)

          should.exist(res.body._id, 'Document id is missing')
          should.equal(res.body.name, _data.post.name, 'Name does not match')
          should.equal(res.body.isStandard, _data.post.isStandard, 'isStandard does not match')

          _data.get.readId = res.body._id
          done()
        })
    })
  })

  describe('# API PATCH /:id', function () {
    it('should partially Update by id', function (done) {
      _data.post.name = 'Dummy - Edited'

      request.patch(`/${_data.get.readId}`).set(config).send(_data.post)
        .expect(200, (err, res) => {
          should.ifError(err)

          should.exist(res.body._id, 'Document id is missing')
          should.equal(res.body.name, _data.post.name, 'Name does not match')
          should.equal(res.body.isStandard, _data.post.isStandard, 'isStandard does not match')
          done()
        })
    })
  })

  describe('# API PUT /:id', function () {
    it('should update by id', function (done) {
      _data.post.name = 'Dummy - Edited 2'
      delete _data.post.account

      request.put(`/${_data.get.readId}`).set(config).send(_data.post)
        .expect(200, (err, res) => {
          should.ifError(err)
          should.exist(res.body._id, 'Document id is missing')
          should.equal(res.body.name, _data.post.name, 'Name does not match')
          should.equal(res.body.isStandard, _data.post.isStandard, 'isStandard does not match')
          done()
        })
    })
  })

  describe('# API GET /:id', function () {
    it('should retrieve record by id - plane', function (done) {
      request.get(`/${_data.get.readId}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should.equal(res.body._id, _data.get.readId)
          should.equal(res.body.name, _data.post.name)
          done()
        })
    })

    it('should retrieve record by id - select', function (done) {
      request.get(`/${_data.get.readId}?${utils.toQueryStr(_data.query.select)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should.exist(res.body.isStandard)
          should.equal(res.body._id, _data.get.readId)
          done()
        })
    })

    it('should retrieve record by id - deselect', function (done) {
      request.get(`/${_data.get.readId}?${utils.toQueryStr(_data.query.deselect)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should.not.exist(res.body.isStandard)
          should.equal(res.body._id, _data.get.readId)
          done()
        })
    })
  })

  describe('# API GET /', function () {
    it('should query or search - plane', function (done) {
      request.get(`${utils.toQueryStr(_data.query.basic)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          done()
        })
    })

    it('should query or search - select', function (done) {
      request.get(`${utils.toQueryStr(_data.query.select)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.exist(res.body.data[0].isStandard)
          done()
        })
    })

    it('should query or search - deselect', function (done) {
      request.get(`${utils.toQueryStr(_data.query.deselect)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.not.exist(res.body.data[0].isStandard)
          done()
        })
    })

    it('should query or search - sort ascending', function (done) {
      request.get(`${utils.toQueryStr(_data.query.sortAsc)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0].name, _data.result.sort[0].name)
          should.equal(res.body.data[1].name, _data.result.sort[1].name)
          done()
      })
    })

    it('should query or search - sort desscending', function (done) {
      request.get(`${utils.toQueryStr(_data.query.sortDesc)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0].name, _data.result.sort[1].name)
          should.equal(res.body.data[1].name, _data.result.sort[0].name)
          done()
        })
    })

    it('should query or search - limit (page 1)', function (done) {
      request.get(`${utils.toQueryStr(_data.query.limit)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0].name, _data.result.sort[0].name)
          done()
        })
    })

    it('should query or search - limit (page 2)', function (done) {
      request.get(`${utils.toQueryStr(_data.query.page)}`).set(config)
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body.data).be.an.Array()
          should(res.body.totalPages).be.a.Number()
          should(res.body.docsPerPage).be.a.Number()
          should(res.body.currentPage).be.a.Number()
          should(res.body.totalRecords).be.a.Number()
          should.equal(res.body.data[0].name, _data.result.sort[1].name)
          done()
        })
    })
  })

  describe('# API DEL', function () {
    it('should delete by id', function (done) {
      request.delete(`/${_data.get.readId}`).set(config)
        .expect(204, done)
    })
  })
})
