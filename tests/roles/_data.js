module.exports = {
  get: {
    readId: '', // to be filled by POST
  },
  post: {
    account: '70db64c8-90ba-52ec-a4c4-bbc64ffd3f4e',
    isStandard: false,
    name: 'Dummy',
    permissions: [{
      accessLevel : 'none', 
      resource : 'accounts'
    }]
  },
  query: {
    basic: {q: 'Dummy'},
    select: {q: 'Dummy', select: 'name isStandard'},
    deselect: {q: 'Dummy', select: '-name -isStandard'},
    sortAsc: {q: 'Dummy', sort: 'name', select: 'name'},
    sortDesc: {q: 'Dummy', sort: '-name', select: 'name'},
    limit: {q: 'Dummy', limit: 1, sort: 'name', select: 'name'},
    page: {q: 'Dummy', limit: 1, sort: 'name', select: 'name', page: 2}
  },
  result: {
    page: [{name: 'xxx'}, {name: 'xxx'}],
    sort: [{name: 'Dummy - Edited 2'}, {name: 'Dummy for search'}]
  }
}